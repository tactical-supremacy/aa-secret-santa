# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - yyyy-mm-dd

### Added

### Changed

### Fixed

## [0.4.0a] - yyyy-mm-dd

### Added

- Delete Applications added on SS on the applications screen, deleting pairs is still not supported as this should be done manually to handle people missing out

### Changed

- AA v4 only
- Updated Templates to new theme

### Fixed

## [0.3.0a] - 2023-12-11

### Added

- Secure Groups integration @pvyparts

### Fixed

- Use recipients main character name on secret santa index

## [0.2.3a] - 2023-12-08

### Changed

- Use User Main Character in discord messages to Santa's @pvyParts

### Fixed

- Fails the Generate Pairs task early in the event of there being one applicant.
- The Applications and Administration buttons were checking against the wrong permission (manage, not manager). These buttons will now appear

## [0.2.2a] - 2023-11-08

### Fixed

- Year is an FK now, not an int

## [0.2.0a] - yyyy-mm-dd

### Added

- discord notify buttons, on santa allocations and outstanding gifts

### Changed

- Years are now rendered as sub templates, no idea why i didnt do this earlier
- real discord DM templates, nothing fancy.

### Fixed

## [0.1.1a] - 2023-10-31

### Fixed

- remove unneeded related_names to reduce conflicts

## [0.1.0a] - 2023-10-31

Initial Minimum Viable Product
